<?php
/**
 * @var Slim\App $app
 */

use App\Http\Controllers\CorporationController;
use App\Http\Controllers\HomeController;
use Slim\Psr7\Request;
use Slim\Psr7\Response;

$app->get('/', HomeController::class . ':home');

$app->post('/corporations', CorporationController::class . ':update');