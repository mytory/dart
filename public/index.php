<?php
use Slim\Factory\AppFactory;
use Slim\Middleware\ContentLengthMiddleware;
use Slim\Middleware\MethodOverrideMiddleware;
use Middlewares\TrailingSlash;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;

require __DIR__ . '/../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/..');
$dotenv->load();

$app = AppFactory::create();

//============ Middleware =============
$twig = Twig::create(realpath(__DIR__ . '/../resources/views'), ['cache' => false]);
$app->add(TwigMiddleware::create($app, $twig));
$app->addBodyParsingMiddleware();
$app->add(new ContentLengthMiddleware());
$app->add(new TrailingSlash(true));
$app->addRoutingMiddleware();
$app->add(new MethodOverrideMiddleware());
$errorMiddleware = $app->addErrorMiddleware(true, true, true);

require '../bootstrap/database.php';
require '../routes/web.php';

$app->run();
