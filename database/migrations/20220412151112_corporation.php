<?php

use Phinx\Migration\AbstractMigration;

class Corporation extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $sql = '
create table corporations (
    id bigint primary key auto_increment,
    name varchar(255),
    code varchar(255),
    stock_code varchar(255),
    created_at timestamp default current_timestamp,
    updated_at timestamp,
    index(name), 
    index(code) 
) charset utf8mb4
        ';
        $this->execute($sql);
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute('drop table corporations');
    }
}
