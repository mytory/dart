<?php

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule;
$capsule->addConnection([
    "driver"   => env('DB_CONNECTION', 'mysql'),
    "host"     => env('DB_HOST', 'localhost'),
    "port"     => env('DB_PORT', '3306'),
    "database" => env('DB_DATABASE', 'dart'),
    "username" => env('DB_USERNAME', 'root'),
    "password" => env('DB_PASSWORD', ''),
]);
$capsule->setAsGlobal();
$capsule->bootEloquent();
