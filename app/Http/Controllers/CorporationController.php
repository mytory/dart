<?php

namespace App\Http\Controllers;

use App\Corporation;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\DB;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ZipArchive;

class CorporationController
{
    public function update(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        global $capsule;

        $capsule->getDatabaseManager()->statement('truncate corporations');

        ini_set('memory_limit', -1);

        $zip_path = tempnam('', '');
        $extract_path = sys_get_temp_dir() . '/' . bin2hex(random_bytes(8));
        mkdir($extract_path, 0777, true);

        $client = new Client();
        $uri = 'https://opendart.fss.or.kr/api/corpCode.xml?crtfc_key=' . env('OPENDART_API_KEY');

        $result = $client->get($uri);
        if ($result->getStatusCode() != 200) {
            throw new \Exception($result->getBody());
        }

        $zip = new ZipArchive;
        file_put_contents($zip_path, $result->getBody());
        if ($zip->open($zip_path)) {
            if (!$zip->extractTo($extract_path)) {
                throw new \Exception('압축 풀다가 실패');
            }
            $zip->close();
        } else {
            throw new \Exception('CORPCODE.xml을 받았는데 Zip 파일이 아닙니다.');
        }

        $xml = simplexml_load_string(file_get_contents($extract_path . '/CORPCODE.xml'), "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $corporations = json_decode($json,TRUE)['list'];
        foreach ($corporations as $corporation_data) {
            $corporation = new Corporation;
            $corporation->name = $corporation_data['corp_name'];
            $corporation->code = $corporation_data['corp_code'];
            $corporation->stock_code = (is_array($corporation_data['stock_code'])) ? implode(',', $corporation_data['stock_code']) : $corporation_data['stock_code'];
            $datetime = date('Y-m-d H:i:s', strtotime($corporation_data['modify_date']));
            $corporation->created_at = $datetime;
            $corporation->updated_at = $datetime;
            $corporation->save();
        }

        return $response->withHeader('location', '/');
    }

}